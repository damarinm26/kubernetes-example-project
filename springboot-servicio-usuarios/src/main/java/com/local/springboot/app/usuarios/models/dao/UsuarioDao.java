package com.local.springboot.app.usuarios.models.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.local.springboot.app.usuarios.models.entity.Usuario;

//@RepositoryRestResource(path = "usuarios")
public interface UsuarioDao extends PagingAndSortingRepository<Usuario, Long>{
	
//	@RestResource(path = "buscar-username")
//	public Usuario findByUsername(@Param("usename") String usename);
//	
	@Query("select u from Usuario u where u.username=?1")
	public Usuario obtenerPorUsername(String username);

}
