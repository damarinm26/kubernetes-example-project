package com.local.springboot.app.usuarios.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.local.springboot.app.usuarios.models.dao.UsuarioDao;
import com.local.springboot.app.usuarios.models.entity.Usuario;

@Service
public class UsuariosServiceImpl implements IUsuariosService{
	
	@Autowired
	UsuarioDao dao;

	@Override
	public Usuario obtenerPorUsername(String username) {
		return dao.obtenerPorUsername(username);
	}

}
