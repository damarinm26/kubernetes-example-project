package com.local.springboot.app.usuarios.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.local.springboot.app.usuarios.models.entity.Usuario;
import com.local.springboot.app.usuarios.service.IUsuariosService;

@RestController
public class UsuariosController {
	
	@Autowired
	IUsuariosService service;
	
	
	@GetMapping("/buscar/{username}")
	public Usuario obtenerUsuario(@PathVariable String username) {
		return service.obtenerPorUsername(username);
	}

}
