package com.local.springboot.app.usuarios.service;

import com.local.springboot.app.usuarios.models.entity.Usuario;

public interface IUsuariosService {
	public Usuario obtenerPorUsername(String username);
}
